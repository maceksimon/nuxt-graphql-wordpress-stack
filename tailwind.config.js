/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */

/**
 * Colors
 */
const colorSetup = {
  ash: {
    hue: 210,
    sat: 15,
  },
  dust: {
    hue: 50,
    sat: 15,
  },
  pollen: {
    hue: 50,
    sat: 75,
  },
  leaf: {
    hue: 160,
    sat: 50,
  },
  fruit: {
    hue: 10,
    sat: 65,
  },
  sky: {
    hue: 210,
    sat: 75,
  },
  bloom: {
    hue: 360,
    sat: 85,
  },
}

const colors = {}

for (const [name, data] of Object.entries(colorSetup)) {
  const colorKeys = [10, 20, 30, 40, 50, 60, 70, 80, 90]
  const colorVals = colorKeys.forEach((key) => {
    if (colors[name] === undefined) {
      colors[name] = {}
    }
    colors[name][key * 10] = `hsl(${data.hue}, ${data.sat}%, ${100 - key}%)`
  })
}

module.exports = {
  purge: [
    './layouts/*.vue',
    './content/*.md',
    './content/**/*.md',
    './pages/*.vue',
    './pages/**/*.vue',
    './pages/**/**/*.vue',
    './components/*.vue',
    './components/**/*.vue',
  ],
  theme: {
    extend: {
      colors: colors,
      fontSize: {
        sm: ['14px', '20px'],
        base: ['16px', '24px'],
        lg: ['20px', '28px'],
        xl: ['24px', '32px'],
      },
      spacing: {
        '7': '1.75rem',
        '72': '18rem',
        '80': '20rem',
        '88': '22rem',
        '96': '24rem',
        '104': '26rem',
        '112': '28rem',
        '120': '30rem',
        '128': '32rem',
      },
    },
  },
}
