# Nuxt + GraphQL + Wordpress

⚠️ Under development ⚠️

!!! Note that this repo is being developed ATM.

This project is a basic template that demonstrates the use of the titular technologies in a single stack. The aim is to create a blog site with user-friendly backend and complete control over the front-end for the developer. This way, the blog should also be ready to deploy as a static site to Netlify.

## Get started

This project assumes that you have a Wordpress installation running on a local server with the WP GraphQL, WPGraphQL Cors and WPGraphQL Tax Query plugins active.

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).
